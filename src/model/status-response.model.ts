import { ApiProperty } from '@nestjs/swagger';

export class StatusResponse {
  @ApiProperty()
  readonly healthy = true;

  @ApiProperty()
  readonly uptime = process.uptime();
}
