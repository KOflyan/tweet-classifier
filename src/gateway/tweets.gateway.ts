import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Client, Server } from 'socket.io';
import { OnEvent } from '@nestjs/event-emitter';
import { EventEnum } from '../enum/event.enum';
import { logger } from '../utils/logger';

@WebSocketGateway({ transports: ['websocket', 'polling'] })
export class TweetsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  private server: Server;

  handleConnection(client: Client): void {
    logger.info(`Client [ ${client.id} ] joined`);
  }

  handleDisconnect(client: Client): void {
    logger.info(`Client [ ${client.id} ] disconnected`);
  }

  @OnEvent(EventEnum.TWEET_RECEIVED_EVENT)
  public onTweetReceived(tweet: Tweet): void {
    this.server.emit(EventEnum.TWEET_RECEIVED_EVENT, tweet);
  }
}
