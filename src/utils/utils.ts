import { NextFunction, Request, Response } from 'express';
import { logger } from './logger';

export class Utils {

  public static capitalize(input: string): string {
    return input
      .split(/(?=[A-Z])/)
      .join('_')
      .toUpperCase();
  }

  public static loggerMiddleware(
    req: Request,
    res: Response,
    next: NextFunction,
  ): void {
    res.once('finish', () =>
      logger.info(`${req.method} ${req.path} ${res.statusCode}`),
    );

    return next();
  }
}
