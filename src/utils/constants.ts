import { join } from 'path';

const constants = {
  DEFAULT_PORT: 3000,
  BASE_URL: '/api',
  MIN_TWEET_LENGTH: 30,
  LANGUAGE_WHITELIST: [
    'en',
    // 'et',
    // 'ru'
  ],
  TWITTER_STREAM_URI: 'https://api.twitter.com/2/tweets/sample/stream?tweet.fields=lang',
  KAFKA: {
    BROKERS: ['kafka:9092'],
    CLIENT_ID: 'tweet-classifier',
    CONSUMER_GROUP_ID: '_consumers',
    TOPICS: {
      TWEETS: 'tweets',
      NEGATIVE_TWEETS: 'negative_tweets',
      POSITIVE_TWEETS: 'positive_tweets',
    },
    PRODUCER_IDS: {
      ANALYSIS_PUBLISHER: 'analysis-publisher',
      TWEET_PUBLISHER: 'tweet-publisher',
    }
  },
  ANALYZER: {
    RESOURCE_PATH: join(__dirname, '../../resources/analyzer'),
    SCRIPT: 'run_sentiment_analysis.py',
    MODEL: 'analyzer_model_v3.pickle'
  },
};

export { constants };
