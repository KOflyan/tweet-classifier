import { EventEmitter2 } from '@nestjs/event-emitter';

export class Queue<T> extends EventEmitter2 {

  private _queue: T[] = [];

  public add(e: T): void {
    this._queue.push(e);
    this.emit('add', this._queue);
  }

  public remove(): T | null {

    if (this.isEmpty()) {
      return null;
    }

    const [e] = this._queue.splice(0, 1);

    this.emit('remove', this._queue);

    return e;
  }

  public peek(): T | null {

    if (this.isEmpty()) {
      return null;
    }

    return this._queue[0];
  }

  public size(): number {
    return this._queue.length;
  }

  public isEmpty(): boolean {
    return this.size() === 0;
  }

  *[Symbol.iterator](): Generator<T> {
    for (const e of this._queue) {
      yield e;
    }
  }
}
