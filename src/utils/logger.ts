import winston from 'winston';
import { config } from '../config/general.config';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.printf((info) => {
      const ts = info.timestamp.slice(0, 19).replace('T', ' ');
      return `${ts} [${info.level}] ${info.message}`;
    }),
  ),
});

if (config.ENV !== 'prod') {
  logger.add(new winston.transports.Console());
} else {
  logger.add(new winston.transports.File({ filename: 'info.log' }));
}

export { logger };
