import { HttpModule, Module, OnModuleInit } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { mergeMap } from 'rxjs/operators';

import { StreamService } from './service/stream/stream.service';
import { TweetsGateway } from './gateway/tweets.gateway';
import { HealthController } from './controller/health.controller';
import { TweetController } from './controller/tweet.controller';
import { KafkaService } from './service/kafka/kafka.service';
import { logger } from './utils/logger';
import { KafkaProducerService } from './service/kafka/kafka-producer.service';
import { KafkaConsumerService } from './service/kafka/kafka-consumer.service';
import { AnalyzerService } from './service/analysis/analyzer.service';
import { EventHandlerService } from './service/event/event-handler.service';

@Module({
  imports: [
    EventEmitterModule.forRoot(),
    HttpModule,
  ],
  controllers: [
    TweetController,
    HealthController,
  ],
  providers: [
    StreamService,
    KafkaService,
    KafkaConsumerService,
    KafkaProducerService,
    EventHandlerService,
    AnalyzerService,
    TweetsGateway
  ],
})
export class AppModule implements OnModuleInit {

  constructor(
    private kafkaService: KafkaService,
    private analyzerService: AnalyzerService,
  ) {}

  onModuleInit(): void {
    logger.info('Connecting to kafka');

    this.kafkaService.connect$()
      .pipe(
        mergeMap(() => this.analyzerService.runAnalysis$())
      )
      .subscribe();
  }
}
