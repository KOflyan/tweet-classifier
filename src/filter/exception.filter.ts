import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { ServerException } from '../exception/exception';

@Catch()
export class ExceptionFilterImpl implements ExceptionFilter {
  // eslint-disable-next-line
  catch(exception: any, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    let status = (exception.getStatus && exception.getStatus()) || 500;

    if (!!exception.response) {
      exception = exception.response;
    }

    if (exception.stack) {
      delete exception.stack;
    }

    if (exception.code && String(exception.code).startsWith('400')) {
      status = 400;
    } else if (status === 500) {
      const e = new ServerException();
      e.message = exception.message || e.message;
      exception = e;
    }

    response.status(status).json({
      timestamp: new Date().toISOString(),
      exception,
    });
  }
}
