import { Utils } from '../utils/utils';

class GenericException extends Error {
  protected constructor(
    public message: string,
    public code?: number,
    public details?: GenericType<string>,
    public name: string = '',
  ) {
    super();
    super.message = message;
    if (!this.name || !this.name.trim().length) {
      this.name = Utils.capitalize(this.constructor.name);
    }
    this.code = code || 500;
  }
}

export class ServerException extends GenericException {
  constructor(message?: string) {
    super(message || 'Unknown exception occurred on server side', 500);
  }
}

export class SecretsConfigurationInvalidException extends GenericException {
  constructor(message?: string) {
    super(message || 'Provided secrets object is invalid', 5001);
  }
}

export class ConfigurationPropertiesInvalidException extends GenericException {
  constructor(message?: string) {
    super(message || 'Provided config object is invalid', 5002);
  }
}

export class StreamReadException extends GenericException {
  constructor(e: Error, message?: string) {
    const details = {
      originalName: e.name,
      originalMessage: e.message,
    };
    super(message || 'Error occurred while reading from stream', 5003, details);
  }
}

export class KafkaConnectionFailedException extends GenericException {
  constructor(message?: string) {
    super(message || 'Connection to kafka failed', 5004);
  }
}

export class NotImplementedException extends Error {
  constructor() {
    super('Method not implemented');
  }
}
