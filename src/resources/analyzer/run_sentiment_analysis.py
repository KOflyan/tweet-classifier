import pickle
import argparse
import re
import time
import json


def extract_features(word_list):
    return dict([(word, True) for word in word_list])


def define_and_process_cmd_args():
    parser = argparse.ArgumentParser("sentiment_analyzer")
    parser.add_argument('text', help='Text to analyze')
    parser.add_argument('model', help='Model name')

    return parser.parse_args()


def run_sentiment_classification(model, text):
    start = time.time()

    with open(model, 'rb') as f:
        classifier = pickle.load(f)

    features = extract_features(text.split())
    prob_dist = classifier.prob_classify(features)

    pred_sentiment = prob_dist.max()

    end = time.time()

    return json.dumps({
        'text': text,
        'probability': round(prob_dist.prob(pred_sentiment), 2),
        'result': prob_dist.max(),
        'time': f"{end - start}"
    })


if __name__ == '__main__':

    args = define_and_process_cmd_args()

    processed_text = re.sub(r"@\w+", '', args.text).strip()

    print(run_sentiment_classification(args.model, processed_text))
