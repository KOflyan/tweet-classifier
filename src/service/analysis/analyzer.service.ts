import { Injectable } from '@nestjs/common';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { ConsumerRunConfig, EachMessagePayload, RecordMetadata } from 'kafkajs';
import { constants } from '../../utils/constants';
import { logger } from '../../utils/logger';
import { KafkaService } from '../kafka/kafka.service';
import { Queue } from '../../utils/ds/queue';
import { filter, bufferTime, tap } from 'rxjs/operators';
import { exec } from 'child_process';
import { v4 } from 'uuid';

@Injectable()
export class AnalyzerService {

  private readonly _script = `${constants.ANALYZER.RESOURCE_PATH}/${constants.ANALYZER.SCRIPT}`;
  private readonly _model = `${constants.ANALYZER.RESOURCE_PATH}/${constants.ANALYZER.MODEL}`;
  private readonly _queue = new Queue<string>();
  // private readonly MAX_PARALLEL_PROCESSES = 5;

  private _queueSubscription: Subscription;


  constructor(
    private kafkaService: KafkaService
  ) {}

  public runAnalysis$(): Observable<void> {
    return this.kafkaService.subscribeToTopic$(
      {
        topic: constants.KAFKA.TOPICS.TWEETS,
        fromBeginning: false
      },
      this._getConsumerRunConfig()
    )
  }

  private _getConsumerRunConfig(): ConsumerRunConfig {
    return {
      eachMessage: this._onMessageReceived.bind(this),
    }
  }

  private _onAnalysisComplete(out: string): void {
    const stdOut = out?.trim();

    if (!stdOut || !stdOut.length) {
      logger.info('No result, wtf?')
      return;
    }

    const output = JSON.parse(stdOut) as AnalysisResult;

    logger.info(`Marked '${output.text}' as '${output.result}' with probability ${output.probability}`);
    logger.info(`Queue size now: ${this._queue.size()}`)

    this._writeResult$(output).subscribe();

  }

//  eslint-disable-next-line @typescript-eslint/no-unused-vars
  private _onAnalysisError(e: Error): void {
    logger.error(`Tweet analysis failed: ${e.message}`);
    logger.error(e);
  }

  private async _onMessageReceived(data: EachMessagePayload): Promise<void> {

    const message = data.message.value?.toString();

    if (!message) {
      return;
    }

    this._queue.add(message);

    if (!this._queueSubscription) {
      this._queueSubscription = this._subscribeToQueue();
      logger.info('Subscribed to queue');
    }
  }

  private _runAnalyzerScript$(message: string | null): void {

    if (!message) {
      logger.info('No message provided; skipping analysis')
      return;
    }

    const cp = exec(`python3 ${this._script} '${message}' '${this._model}'`);
    logger.info('Running analysis...')

    cp.on('error', this._onAnalysisError.bind(this));

    if (!cp.stdout) {
      return;
    }
    cp.stdout.on('data', this._onAnalysisComplete.bind(this));
  }

  private _subscribeToQueue(): Subscription {
    return fromEvent(this._queue, 'add')
      .pipe(
        bufferTime(500, null, 20),
        filter(() => !this._queue.isEmpty()),
        tap(() => this._runAnalyzerScript$(this._queue.remove()))
      )
      .subscribe()
  }

  private _writeResult$(analysisResult: AnalysisResult): Observable<RecordMetadata[] | null> {
    const topic = `${analysisResult.result}_${constants.KAFKA.TOPICS.TWEETS}`;

    return this.kafkaService.send$(
      topic,
      {
        key: v4(),
        value: JSON.stringify({ text: analysisResult.text, probability: analysisResult.probability }),
      },
      constants.KAFKA.PRODUCER_IDS.ANALYSIS_PUBLISHER,
    );
  }
}

