import { Injectable } from '@nestjs/common';
import { CompressionTypes, Message, Producer, RecordMetadata } from 'kafkajs';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { v4 } from 'uuid';
import { kafkaConfig } from '../../config/kafka.config';
import { GenericKafkaClientService } from './generic-kafka-client.service';

@Injectable()
export class KafkaProducerService extends GenericKafkaClientService<Producer>{

  public add$(
    id: string = v4(),
    config = kafkaConfig.PRODUCER_CONFIG
  ): Observable<Producer> {
    const producer = kafkaConfig.KAFKA.producer(config);
    this._clients[id] = producer;

    return from(producer.connect())
      .pipe(map(() => producer));
  }

  public send$(
    topic: string,
    messages: Message[],
    id?: string,
    compression = CompressionTypes.GZIP
  ): Observable<RecordMetadata[]> {

    return from(this.getClient(id).send({
      topic,
      messages,
      compression
    }));
  }
}
