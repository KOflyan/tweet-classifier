import { Injectable } from '@nestjs/common';
import { Consumer, ConsumerRunConfig, ConsumerSubscribeTopic, Message, Producer, RecordMetadata } from 'kafkajs';
import { constants } from '../../utils/constants';
import { BehaviorSubject, forkJoin, from, Observable } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { logger } from '../../utils/logger';
import { KafkaConsumerService } from './kafka-consumer.service';
import { KafkaProducerService } from './kafka-producer.service';
import { kafkaConfig } from '../../config/kafka.config';

@Injectable()
export class KafkaService {

  private _ready$ = new BehaviorSubject(false);

  constructor(
    private kafkaConsumerService: KafkaConsumerService,
    private kafkaProducerService: KafkaProducerService,
  ) {}

  public send$(
    topic: string,
    messages: Message[] | Message,
    id: string
  ): Observable<RecordMetadata[] | null> {

    if (!Array.isArray(messages)) {
      messages = [messages];
    }

    return this.kafkaProducerService.send$(topic, messages, id);
  }

  public subscribeToTopic$(
    subscriptionConf: ConsumerSubscribeTopic,
    runConf: ConsumerRunConfig
  ): Observable<void> {
    return this.kafkaConsumerService
      .subscribeToTopic$(subscriptionConf)
      .pipe(
        mergeMap(() => this.kafkaConsumerService.run$(runConf))
      );
  }

  public connect$(): Observable<(Producer | Consumer)[]> {
    return this._createTopics$()
      .pipe(
        mergeMap(() => this._addProducersAndConsumers$()),
        tap(() => this._ready$.next(true)),
        tap(() => logger.info('Successfully connected to kafka'))
      );
  }

  private _addProducersAndConsumers$(): Observable<(Producer | Consumer)[]> {
    return forkJoin([
      this.kafkaProducerService.add$(constants.KAFKA.PRODUCER_IDS.TWEET_PUBLISHER),
      this.kafkaProducerService.add$(constants.KAFKA.PRODUCER_IDS.ANALYSIS_PUBLISHER),
      this.kafkaConsumerService.add$()
    ])
  }

  private _createTopics$(): Observable<void> {
    const topics = Object.values(constants.KAFKA.TOPICS)
      .map(topic => ({
        topic,
        numPartitions: 10,
        replicationFactor: 2,
      }));

    const admin = kafkaConfig.KAFKA.admin();

    return from(admin.connect())
      .pipe(
        mergeMap(() => admin.createTopics({
          topics
        })),
        mergeMap(() => admin.disconnect())
      );
  }
}
