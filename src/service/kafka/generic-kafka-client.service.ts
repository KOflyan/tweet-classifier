import { Consumer, ConsumerConfig, Producer, ProducerConfig } from 'kafkajs';
import { Observable } from 'rxjs';
import { ServerException } from '../../exception/exception';

export abstract class GenericKafkaClientService<T extends Producer | Consumer> {

  protected _clients: KafkaClients<T> = {};

  public abstract add$(
    id: string,
    config?: ProducerConfig | ConsumerConfig
  ): Observable<T>;

  public remove(id: string): void {
    delete this._clients[id];
  }

  public getClient(id?: string): T {

    if (!id && Object.keys(this._clients).length) {
      return Object.values(this._clients)[0];
    }

    if (!id || !this._clients.hasOwnProperty(id)) {
      throw new ServerException(`No clients found for the id [ ${id} ]`)
    }
    return this._clients[id];
  }

  get clients(): KafkaClients<T> {
    return this._clients;
  }
}
