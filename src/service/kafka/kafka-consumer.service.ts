import { Injectable } from '@nestjs/common';
import { v4 } from 'uuid';
import { kafkaConfig } from '../../config/kafka.config';
import { from, Observable } from 'rxjs';
import { Consumer, ConsumerRunConfig, ConsumerSubscribeTopic } from 'kafkajs';
import { map } from 'rxjs/operators';
import { GenericKafkaClientService } from './generic-kafka-client.service';

@Injectable()
export class KafkaConsumerService extends GenericKafkaClientService<Consumer>{

  public add$(
    id: string = v4(),
    config = kafkaConfig.CONSUMER_CONFIG
  ): Observable<Consumer> {
    const consumer = kafkaConfig.KAFKA.consumer(config);
    this._clients[id] = consumer;

    return from(consumer.connect())
      .pipe(map(() => consumer));
  }

  public subscribeToTopic$(
    subscriptionConf: ConsumerSubscribeTopic,
    id?: string
  ): Observable<void> {
    return from(
      this.getClient(id).subscribe(subscriptionConf)
    );
  }

  public run$(
    runConfig: ConsumerRunConfig,
    id?: string
  ): Observable<void> {
    return from(
      this.getClient(id).run(runConfig)
    );
  }
}
