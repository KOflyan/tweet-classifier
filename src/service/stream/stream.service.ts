import { AxiosResponse } from 'axios';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import split from 'split';
import { Readable } from 'stream';

import { HttpService, Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { secrets } from '../../config/secrets.config';
import { constants } from '../../utils/constants';
import { EventEnum } from '../../enum/event.enum';
import { StreamReadException } from '../../exception/exception';
import { logger } from '../../utils/logger';

@Injectable()
export class StreamService {

  private _stream: Readable | null = null;

  constructor(
    private http: HttpService,
    private eventEmitter: EventEmitter2
  ) {}

  public openStream(): void {
    logger.info('Opening stream');

    this.connect$()
      .pipe(
        mergeMap((res) => of(this.readFromStream(res))),
      )
      .subscribe(
        () => logger.info('Stream opened successfully'),
        (e) => {
          logger.error('Error occurred while opening stream:', e);
          this.closeStream();
          this._stream = null;
        },
      );
  }

  public closeStream(): void {
    logger.info('Ending stream');
    this.stream?.destroy();
  }

  public readFromStream(res: AxiosResponse): void {
    this._stream = res.data.pipe(split());

    if (!this._stream) {
      return;
    }

    this._stream.on('data', this._onData.bind(this));

    this._stream.on('error', (e) => {
      throw new StreamReadException(e);
    });

    this._stream.on('end', () => {
      logger.info('Stream ended successfully')
    });
  }

  private connect$(): Observable<AxiosResponse> {
    return this.http.get(constants.TWITTER_STREAM_URI, {
      responseType: 'stream',
      headers: {
        Authorization: `Bearer ${secrets.BEARER_TOKEN}`,
      },
    });
  }

  private _onData(line: string): void {
    let tweet;

    try {
      tweet = JSON.parse(line) as Tweet;
    } catch (e) {
      logger.error('Couldn\'t convert message to json, dropping');
      return;
    }

    if (this._shouldProcessTweet(tweet)) {
      this.eventEmitter.emit(EventEnum.TWEET_RECEIVED_EVENT, tweet);
      return;
    }

    logger.info(`Dropping tweet: ${tweet?.data?.text}`)
  }

  private _shouldProcessTweet(tweet: Tweet): boolean {

    if (!tweet?.data) {
      return false;
    }

    const noLangOrSupportedLang = !tweet.data.lang || constants.LANGUAGE_WHITELIST.includes(tweet.data.lang);
    const isLongEnough = tweet.data.text?.length >= constants.MIN_TWEET_LENGTH;

    return [
      noLangOrSupportedLang,
      isLongEnough
    ].every(Boolean);
  }

  get stream(): Readable | null {
    return this._stream;
  }
}
