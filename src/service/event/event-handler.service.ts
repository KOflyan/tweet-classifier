import { OnEvent } from '@nestjs/event-emitter';
import { EventEnum } from '../../enum/event.enum';
import { constants } from '../../utils/constants';
import { KafkaService } from '../kafka/kafka.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class EventHandlerService {

  constructor(
    private kafkaService: KafkaService
  ) {
  }

  @OnEvent(EventEnum.TWEET_RECEIVED_EVENT)
  public onTweetReceived(tweet: Tweet): void {
    const parsedTweet = tweet?.data || {};
    const message = [
      {
        key: parsedTweet.id,
        value: parsedTweet.text
      },
    ];

    this.kafkaService.send$(
      constants.KAFKA.TOPICS.TWEETS,
      message,
      constants.KAFKA.PRODUCER_IDS.TWEET_PUBLISHER
    )
  }
}
