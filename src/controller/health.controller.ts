import { Controller, Get } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { StatusResponse } from '../model/status-response.model';

@ApiTags('Health')
@Controller('/health')
export class HealthController {

  @ApiResponse({
    status: 200,
    description: 'Service is running.',
    type: StatusResponse,
  })
  @Get('/')
  public status(): StatusResponse {
    return new StatusResponse();
  }
}
