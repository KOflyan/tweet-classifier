import { Response } from 'express';
import path from 'path';

import { Controller, Get, Post, Res } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { StreamService } from '../service/stream/stream.service';

@ApiTags('Tweet')
@Controller('/tweet')
export class TweetController {

  constructor(
    private streamService: StreamService,
  ) {}

  @ApiResponse({
    status: 200,
    description: 'View the stream',
  })
  @Get('/view')
  public view(@Res() res: Response): void {
    return res.sendFile(path.join(__dirname, '../../static/index.html'));
  }

  @ApiResponse({
    status: 204,
    description: 'Start reading from stream',
  })
  @Post('/startStream')
  public startReadStream(): void {
    this.streamService.openStream();
  }

  @ApiResponse({
    status: 204,
    description: 'Close the stream',
  })
  @Post('/endStream')
  public endReadStream(): void {
    this.streamService.closeStream();
  }
}
