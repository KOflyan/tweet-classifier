import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { config } from './config/general.config';
import { constants } from './utils/constants';
import { Utils } from './utils/utils';

(async function bootstrap(): Promise<void> {
  const isProd = config.ENV === 'prod';

  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    bodyParser: true,
  });
  app.enableCors();
  // app.use(helmet());
  app.disable('x-powered-by');
  app.setGlobalPrefix(constants.BASE_URL);
  app.useGlobalPipes(new ValidationPipe());

  if (!isProd) {
    app.use(Utils.loggerMiddleware);
  }

  const options = new DocumentBuilder()
    .setTitle(config.APP_NAME)
    .setDescription('Tweet parser')
    .setVersion(config.VERSION)
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(`${constants.BASE_URL}/docs`, app, document);
  await app.listen(config.PORT);
})();
