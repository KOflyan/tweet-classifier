import { SecretsConfigurationInvalidException } from '../exception/exception';

const secrets: SecretsModel = {
  API_KEY: process.env.API_KEY,
  API_SECRET: process.env.API_SECRET,
  BEARER_TOKEN: process.env.BEARER_TOKEN,
};

Object.entries(secrets).forEach(([k, v]) => {
  if (!v) {
    console.error(`${k} must be specified!`);
    throw new SecretsConfigurationInvalidException();
  }
});

export { secrets };
