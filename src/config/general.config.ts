import * as pkg from '../../package.json';
import { constants } from '../utils/constants';

const config: ConfigModel = {
  APP_NAME: pkg.name,
  VERSION: pkg.version,
  ENV: (process.env.NODE_ENV as Environment) || 'dev',
  PORT: Number(process.env.PORT) || constants.DEFAULT_PORT,
};

export { config };
