import { constants } from '../utils/constants';
import { Kafka } from 'kafkajs';

const kafkaConfig = {

  KAFKA: new Kafka({
    brokers: constants.KAFKA.BROKERS,
    clientId: constants.KAFKA.CLIENT_ID
  }),

  PRODUCER_CONFIG: {
    allowAutoTopicCreation: false,
  },

  CONSUMER_CONFIG: {
    groupId: constants.KAFKA.CONSUMER_GROUP_ID,
    allowAutoTopicCreation: false,
  },
}

export {
  kafkaConfig,
};
