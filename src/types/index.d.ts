declare type Environment = 'dev' | 'prod';

declare type GenericType<K extends string | number, V = string> = {
  [key in K]: V;
};

declare type ConfigModel = {
  APP_NAME: string;
  VERSION: string;
  ENV: Environment;
  PORT: number;
};

declare type SecretsModel = {
  API_KEY?: string;
  API_SECRET?: string;
  BEARER_TOKEN?: string;
};

declare type Tweet = {
  data: {
    id: string;
    text: string;
    lang?: string;
  }
}

declare type KafkaClients<T> = {
  [key: string]: T;
}

declare type AnalysisResult = {
  text: string;
  result: 'positive' | 'negative';
  probability: number;
  time: number;
}

