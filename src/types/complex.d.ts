import { Message, Producer } from 'kafkajs';

interface MessageData {
  topic: string;
  messages: Message[];
}

