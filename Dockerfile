FROM node:15-buster-slim

ENV NODE_PATH=/app/dist \
    NODE_ENV=prod

WORKDIR /app

RUN apt update \
  && apt upgrade -y \
  && apt install python3 python3-pip -y

RUN pip3 install \
  nltk

COPY package.json /app/package.json
RUN npm i

COPY . /usr/app

RUN npm run build

CMD ["node", "dist/src/main.js"]
