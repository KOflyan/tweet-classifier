WIP
===


Overview
--------
----
Stack
* RxJS
* Kubernetes + Helm
* NestJS
* Kafka
* A bit of Python for sentiment analysis

Prerequisites

* Helm
* Kubectl
* Docker


This project is purely educational and has no real life value.
I built it to play around with kafka and kubernetes.

The general idea of the project is to:

1) Using Twitter API, open stream to receive realtime tweets;
2) Collect the tweets matching custom filters (language + length);
3) Save tweets to Kafka `tweets` topic;
4) Classify each received tweet as either `positive` or `negative` and write it along with the classification result
   to the corresponding Kafka topic.

Additionally:

1) Expose a websocket connection to forward the tweets from the application itself
2) Use docker, kubernetes & helm to easily rollout all needed resources


----

Using the app
---------------

To start or restart, simply do

```bash
cd tweet-classifier

./start.sh
```
This will spin up the environment and expose 2 services:

API - http://tweet-classifier.local

Kafka UI - http://kafka-ui.local

API documentation (Swagger) is available at: http://tweet-classifier.local/api/docs

Removing the app
---------------

Open the `hosts` file

```bash
sudo vim /etc/hosts
```
And remove the block starting with this comment

```
# Added by tweet-classifier app
```

Then simply delete the `tweet-classifier` folder.
