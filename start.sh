#!/bin/bash

set -e

trap "exit" INT

project_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"

cd "$project_dir" || exit

app_release_name="tweet-classifier"
image_name="local/$app_release_name"
hosts_file_path="/etc/hosts"
env=$NODE_ENV
file="Dockerfile"

ingress_dependency_path="https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/cloud/deploy.yaml"
dev_hosts="
# Added by tweet-classifier app
# <>
127.0.0.1 tweet-classifier.local
127.0.0.1 kafka-ui.local
# </>
"

if ! grep -q "tweet-classifier" "$hosts_file_path"; then
  printf "Require sudo to add hostnames needed for development\n"
  echo "$dev_hosts" | sudo tee -a "$hosts_file_path" > /dev/null
  printf "Successfully modified hosts file\n\n"
fi


if [[ -z "$env" || "$env" == "dev" ]]; then
  env="dev"
  file="Dockerfile.dev"
fi


if [[ -z $(docker image ls | grep "$image_name") ]]; then
  printf "Building tweet-classifier image...\n\n"
  docker build . -f "$file" -t local/tweet-classifier
  printf "\nImage built.\n"
fi


if [[ -z $(kubectl get namespace | grep -i -w -E 'ingress-nginx|nginx-ingress') ]]; then
  printf "Installing nginx ingress controller...\n\n"
  kubectl apply -f "$ingress_dependency_path"
fi


if [[ -n $(helm list | grep "$app_release_name") ]]; then
  pods="start"

  helm uninstall "$app_release_name"

  while [[ -n "$pods" ]]; do
    printf "Waiting for the previous release to terminate...\n"
    pods=$(kubectl get pods --ignore-not-found)
    sleep 5
  done
fi

printf "Triggering Helm release...\n\n"
helm install "$app_release_name" .deploy/ \
  --set tweetClassifier.volume.hostPath="$project_dir"

printf "\n\nDone."
